const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const morgan = require('morgan')
const debug = require('debug')('chart-sample:server')
const http = require('http')
const indexRouter = require('./routes/index')
const dataRouter = require('./routes/data')
const log4js = require('log4js')
const crawler = require('./binary-crawler')

const loggerLayout = {
  type: 'pattern',
  pattern: '%d{yyyy-MM-dd hh:mm:ss.SSS} %p [%c] %m',
}

log4js.configure({
  appenders: {
    console: { type: 'console', layout: loggerLayout },
    file: { type: 'dateFile', filename: 'app.log', layout: loggerLayout },
  },
  categories: {
    default: { appenders: ['console', 'file'], level: 'debug' },
    crawler: { appenders: ['console', 'file'], level: 'debug' },
  },
})

const logger = log4js.getLogger()

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// app.use(morgan('combined'));
app.use(log4js.connectLogger(log4js.getLogger('http'), { level: 'auto' }))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/data', dataRouter)

app.get('/time', (req, res) => {
  res.send({ serverTime: new Date().getTime() })
})

app.get('/client/count', (req, res) => {
  res.send({ count: Object.keys(clientList).length })
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '80')
app.set('port', port)

/**
 * Create HTTP server.
 */
const server = http.createServer(app)
const io = require('socket.io')(server)
const clientList = {}
server.listen(port)
server.on('error', onError)
server.on('listening', onListening)
io.on('connection', (client) => {
  clientList[client.id] = client
  logger.info('client connection : ', client.id)
  client.on('disconnect', () => {
    logger.info('client disconnected : ', client.id)
    delete clientList[client.id]
  })
})

function Interval(duration, fn) {
  this.baseline = undefined
  this.first = true
  this.run = function () {
    if (this.baseline === undefined) {
      this.baseline = new Date().getTime()
    }
    fn(this.baseline)
    const end = new Date().getTime()
    if (!this.first) {
      this.baseline += duration
    }

    let nextTick = duration - (end - this.baseline)
    if (nextTick < 0) {
      nextTick = 0
    }
    this.first = false;
    (function (i) {
      i.timer = setTimeout(function () {
        i.run(end)
      }, nextTick)
    }(this))
  }

  this.stop = function () {
    clearTimeout(this.timer)
  }
}

async function startCrawling() {
  await crawler.beforeCrawling()
  const timer = new Interval(1000, async (baseTime) => {
    await crawler.startCrawling(baseTime)
    io.sockets.emit('priceData', crawler.getPriceData())
    // await crawler.increaseCount();
    logger.info('Counter time : ', baseTime, new Date(baseTime))
    logger.info('Connected client count : ', Object.keys(clientList).length)
    logger.info('Current server time : ', new Date())
    logger.info(`===================== end crawling : ${crawler.getPriceData().count}, ${crawler.getPriceData().reqTime} ====================\n`)
  })
  timer.run()
}

let isStartCrawling = false
const startInterval = setInterval(async () => {
  const currDate = new Date()
  if (currDate.getMinutes() % 2 === 0 && currDate.getSeconds() === 2) {
    if (!isStartCrawling) {
      isStartCrawling = true
      logger.info('start watching')
      await startCrawling()
    }
    clearInterval(startInterval)
  }
}, 123)

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      logger.error(bind + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      logger.error(bind + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address()
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  console.log('Listening on ' + bind)
}
