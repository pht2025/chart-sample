const express = require('express');
const crawler = require('../binary-crawler');
const router = express.Router();

// noinspection JSUnresolvedFunction
router.get('/', function(req, res) {
	res.send(crawler.getPriceData());
});

module.exports = router;
