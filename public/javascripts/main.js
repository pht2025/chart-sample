App.main = (function() {
  am4core.useTheme(am4themes_animated);
  am4core.useTheme(am4themes_dark);

  $('#main .base_container .base.circle.bg').click(function() {
    $('#main .base_container .base_value_label').toggle();
  });
  $('#main .result_container .result.circle.bg').click(function() {
    $('#main .result_container .result_value_label').toggle();
  });

  return {
    count: -1,
    basePoint: 60,
    breakTime: 10,
    startPointBaseValue: 0,
    endPoint: 120,
    endPointBaseValue: 0,
    mainChart: undefined,
    // baseChart: undefined,
    // resultChart: undefined,
    timeInterval: undefined,
    isBreakTime: false,
    prevData: undefined,
    resultLine: undefined,
    skipDone: false,
    disposeAllChartDone: false,
    gameOverFlag: false,

    start: function() {
      $('#chart-wrap').hide();
      $('#main-wrap').show();
      // if (navigator.platform.toLowerCase() === 'win32') {
      // 	if (!window.sessionStorage.getItem('5fe0f262363b515a31f074dfc76d6351')) {
      // 		App.DummyChart.init();
      // 		return;
      // 	}
      // }
      var socket = io.connect(location.href);
      socket.on('priceData', this.dataReceivedHandler.bind(this));
    },

    /**
     * Websocket Data 수신 핸들러.
     * @param priceData object
     * @param priceData.avgPrice number
     * @param priceData.maxPrice number
     * @param priceData.minPrice number
     * @param priceData.biPrice number
     * @param priceData.ixPrice number
     * @param priceData.count number
     * @param priceData.startCheck number
     * @param priceData.startBaseValue number
     * @param priceData.resultBaseValue number
     * @param priceData.openAvg number
     * @param priceData.resultAvg number
     * @param priceData.openBi number
     * @param priceData.openIx number
     * @param priceData.resultBi number
     * @param priceData.resultIx number
     * @param priceData.state number NONE, START, OPEN, RESULT
     */
    dataReceivedHandler: function(priceData) {
      // App.main.startTimer();
      console.log(priceData);

      if (!this.skipDone && this.gameOverFlag) {
        this.skipDone = true;
        return;
      }

      if (!this.disposeAllChartDone && this.gameOverFlag) {
        this.disposeAllChart();
        this.disposeAllChartDone = true;
      }

      if (priceData.count > 11) {
        this.gameOverFlag = false;
      }

      if (priceData.count >= 11 && !this.screenClearFlag) {
        this.screenClear();
        this.screenClearFlag = true;
      }

      if (priceData.count >= this.basePoint && priceData.count < this.endPoint) {
        if (priceData.openBi === 0 || priceData.openIx === 0) {
          console.log('******************** OPEN DigiFinex OR IDAX data is empty');
          priceData.avgPrice = 0;
          priceData.openAvg = 0;
          this.showServerCommunicationErrorOverlay();
          this.count = priceData.count;
          if (priceData.count > 67 && this.prevData) {
            this.updateCurrentValueElem({ avgPrice: 0, biPrice: 0, ixPrice: 0 });
            var newData = {
              point: this.count,
              avgValue: this.prevData.avgPrice,
              townSize: 0,
              binValue: this.prevData.biPrice,
              idxValue: this.prevData.ixPrice,
              startBaseTime: this.prevData.count,
              endBaseTime: this.prevData.count,
              startPointBaseValue: this.startPointBaseValue,
              endBaseValue: this.endPointBaseValue,
            };

            if (this.mainChart) {
              this.mainChart.addData(newData, true);
            }
          }
          return;
        } else {
          this.hideMainOverlay();
        }
      }

      if (priceData.count === this.endPoint || priceData.count < 11) {
        if (priceData.resultBi === 0 || priceData.resultIx === 0) {
          console.log('******************** RESULT DigiFinex OR IDAX data is empty');
          this.count = priceData.count;
          priceData.avgPrice = 0;
          priceData.resultAvg = 0;
          this.showServerCommunicationErrorOverlay();
          if (priceData.count > 7) {
            this.clearOverUnderIndicator();
            this.gameOver(priceData);
          }
          return;
        }
      }

      this.updateCurrentValueElem(priceData);
      this.updateOverUnderLabel(priceData);
      this.updateOverUnderIndicator(priceData);
      this.updateBaseLineFlag(priceData);
      this.updateBetLimitLineFlag(priceData);
      this.updateResultLineFlag(priceData);

      // if (!this.hasNotMainChart() && priceData.count < 11 && !this.gameOverFlag) {
      //   this.disposeAllChart();
      //   this.hideMainOverlay();
      //   this.gameOverFlag = true;
      //   this.screenClearFlag = false;
      // }

      if (priceData.avgPrice <= 0) {
        return;
      }
      // var breakTimeLeft = 180 - priceData.count;

      // if ((priceData.state === App.BIN_PROC.START || priceData.state === App.BIN_PROC.OPEN) && this.isBreakTime) {
      //   this.isBreakTime = false;
      // }

      // if (!this.isBreakTime && (priceData.count <= this.breakTime)) {
      //   this.isBreakTime = true;
        // if (breakTimeLeft <= 30) {
        //   this.showProgressImage(breakTimeLeft);
        // } else {
        // this.startPointBaseValue = priceData.startBaseValue;
        // this.endPointBaseValue = priceData.openAvg;
        // this.updateTimeElemValue(priceData);
        // this.showBaseValueOverlay(priceData);
        // this.gameOver(priceData);
        // }
      // }

      // if (this.isBreakTime) {
      //   this.count = -1;
      //   return;
      // }

      // if (this.count !== -1 && this.count + 1 !== priceData.count && this.prevData) {
      //   this.prevData.count = this.count + 1;
      //   this.updateData(this.prevData);
      // }

      this.count = priceData.count;

      if (this.startPointBaseValue === 0) {
        this.startPointBaseValue = priceData.startBaseValue;
      }

      if (this.hasNotMainChart()) {
        this.updateCurrentMinutesElem(priceData);
        // this.screenClear();
        this.resetState();
        this.prepareMainChart(priceData);
        // this.prepareBaseChart(priceData);

        if (priceData.count > 0 && priceData.count < this.basePoint) {
          this.startPointBaseValue = priceData.startBaseValue;
          // this.baseChart.setBasePointValue(this.startPointBaseValue);
        }

        if (priceData.count < 40 && priceData.count > 30) {
          this.activeBetLimitIcon();
        }

        if (priceData.count === this.basePoint) {
          this.endPointBaseValue = priceData.openAvg;

          setTimeout(function() {
            this.showBaseValueOverlay(priceData);
          }.bind(this), 1);

          setTimeout(function() {
            this.showOverUnderIndicator(priceData);
          }.bind(this), 1);

          setTimeout(function() {
            if (!this.resultLine) {
              this.resultLine = this.mainChart.makeResultLine(priceData.openAvg);
            }
          }.bind(this), 1);
        }

        if (priceData.count > this.basePoint) {
          if (this.endPointBaseValue === 0) {
            this.endPointBaseValue = priceData.openAvg;
          }
          if (!this.resultLine) {
            this.resultLine = this.mainChart.makeResultLine(priceData.openAvg);
          }
          this.showBaseValueOverlay(priceData);
          this.activeBaseLineIcon();
          this.showOverUnderIndicator(priceData);
          // this.prepareResultChart(priceData);
        }

        if (priceData.count === 1) {
          this.mainChart.addData({
            point: 0,
            avgValue: priceData.avgPrice,
            townSize: this.isBasePointCount() ? 7 : 0,
            binValue: priceData.biPrice,
            idxValue: priceData.ixPrice,
            startBaseTime: 0,
            endBaseTime: 0,
            startPointBaseValue: this.startPointBaseValue,
            endBaseValue: this.endPointBaseValue,
          }, true);
        }
        this.mainChart.addData({
          point: priceData.count,
          avgValue: priceData.avgPrice,
          townSize: this.isBasePointCount() ? 7 : 0,
          binValue: priceData.biPrice,
          idxValue: priceData.ixPrice,
          startBaseTime: 0,
          endBaseTime: 0,
          startPointBaseValue: this.startPointBaseValue,
          endBaseValue: this.endPointBaseValue,
        }, true);
      } else {
        this.updateData(priceData);
      }

      this.prevData = priceData;
    },

    screenClear: function() {
      this.screenClearFlag = true;
      this.hideMainOverlay();
      this.hideBreakTimeOverlay();
      this.clearTimeLabel();

      this.clearResultValueLabel();
      this.clearBaseValueLabel();
      this.clearOverUnderIndicator();
      this.clearGameOverFlag();
      this.clearAnimationFlag();
    },

    clearTimeLabel: function() {
      // document.querySelector('#main_overlay .time_label.start_time > span').textContent = '';
      // document.querySelector('#main_overlay .time_label.base_time > span').textContent = '';
      // document.querySelector('#main_overlay .time_label.end_time > span').textContent = '';
    },

    showOverUnderIndicator: function() {
      $('#main .chart_container .top_overlay').show();
    },

    showOverUnderLabel: function() {
      $('#main .chart_container .result_label').show('slow');
    },

    clearBaseValueLabel: function() {
      $('#main .bottom .base_value_label .value').text('00000.00');
      $('#main .bottom .base_value_label').hide();
    },
    clearResultValueLabel: function() {
      $('#main .bottom .result_value_label .value').text('00000.00');
      $('#main .bottom .result_value_label').hide();
    },
    clearOverUnderIndicator: function() {
      $('#main .chart_container .top_overlay .circle').removeClass('active');
      $('#main .chart_container .top_overlay .circle').removeClass('positive');
      $('#main .chart_container .top_overlay .circle').removeClass('negative');
      $('#main .chart_container .top_overlay .circle').addClass('disable');
      $('#main .chart_container .top_overlay').hide();
      $('#main .chart_container .result_label').text('-');
      $('#main .chart_container .result_label').hide();
    },

    clearGameOverFlag: function() {
      $('#main_chart_div').removeClass('game_over');
      $('#main .row.bottom').removeClass('game_over');
    },

    clearAnimationFlag: function() {
      $('.circle').removeClass('active');
      $('#main .bottom .bet_limit.circle.bg').removeClass('disable');
      $('#main .bottom .base.circle.bg').removeClass('enable');
      $('#main .bottom .result.circle.bg').removeClass('enable');
    },

    updateTimeElemValue: function(priceData) {
      if (priceData.startTime && document.querySelector('#main_overlay .time_label.start_time > span').textContent === '') {
        // var start = moment(priceData.startTime).format('HH:mm');
        // var open = moment(priceData.startTime).add(1, 'minutes').format('HH:mm');
        // var result = moment(priceData.startTime).add(2, 'minutes').format('HH:mm');
        // document.querySelector('#main_overlay .time_label.start_time > span').textContent = start;
        // document.querySelector('#main_overlay .time_label.base_time > span').textContent = open;
        // document.querySelector('#main_overlay .time_label.end_time > span').textContent = result;
      }
    },

    updateData: function(priceData) {
      this.updateTimeElemValue(priceData);

      if (this.count === 2) {
        this.mainChart.addData({
          point: 0,
          avgValue: this.prevData.avgPrice,
          townSize: this.isBasePointCount() ? 7 : 0,
          binValue: this.prevData.biPrice,
          idxValue: this.prevData.ixPrice,
          startBaseTime: 0,
          endBaseTime: 0,
          startPointBaseValue: this.startPointBaseValue,
          endBaseValue: this.endPointBaseValue,
        }, true);
      }

      // if (this.isCloseRangeCount()) {
      //   this.mainChart.makeCloseRange();
      // }

      if (this.isBetLimitAlertCount()) {
        this.activeBetLimitIcon();
      }

      if (this.isBetLimitOver()) {
        this.disableBetLimitIcon();
      }

      if (this.isBasePointCount()) {
        this.endPointBaseValue = priceData.openAvg;

        setTimeout(function() {
          this.showBaseValueOverlay(priceData);
          this.activeBaseLineIcon();
          this.showOverUnderIndicator(priceData);
        }.bind(this), 1);

        // setTimeout(function() {
        //   this.addResultChart(priceData);
        // }.bind(this), 1);

        setTimeout(function() {
          if (!this.resultLine) {
            this.resultLine = this.mainChart.makeResultLine(priceData.openAvg);
          }
        }.bind(this), 1);
      }
      if (this.isResultAreaCount()) {
        if (!this.resultLine) {
          if (this.endPointBaseValue === 0) {
            this.endPointBaseValue = priceData.openAvg;
          }
          setTimeout(function() {
            this.showBaseValueOverlay(priceData);
            this.activeBaseLineIcon();
            this.showOverUnderIndicator(priceData);
          }.bind(this), 1);
          this.resultLine = this.mainChart.makeResultLine(priceData.openAvg);
        }
        this.mainChart.updateResultLineData(priceData.avgPrice, this.endPointBaseValue);
      }

      var newData = {
        point: this.count,
        avgValue: priceData.avgPrice,
        townSize: this.isBasePointCount() ? 7 : 0,
        binValue: priceData.biPrice,
        idxValue: priceData.ixPrice,
        startBaseTime: this.count,
        endBaseTime: this.count,
        startPointBaseValue: this.startPointBaseValue,
        endBaseValue: this.endPointBaseValue,
      };

      this.mainChart.addData(newData, true);

      // if (this.isResultAreaCount()) {
      // this.resultChart.setBasePointValue(this.endPointBaseValue);
      // this.resultChart.updateData(priceData);

      // this.baseChart.updateMaxValue(this.mainChart.getMaxValue());
      // this.baseChart.updateMinValue(this.mainChart.getMinValue());
      // } else {
      // this.baseChart.setBasePointValue(this.startPointBaseValue);
      // this.baseChart.updateData(priceData);
      // }

      if (this.isLastCount() || (!this.gameOverFlag && this.count < 8)) {
        console.log('game over', priceData);
        setTimeout(function() {
          this.gameOver(priceData);
        }.bind(this), 1);
      }
    },

    gameOver: function(priceData) {
      console.log('gameOver', this.gameOverFlag, priceData);
      if (this.gameOverFlag) return;
      this.skipDone = false;
      this.disposeAllChartDone = false;
      this.screenClearFlag = false;
      this.gameOverFlag = true;
      this.isBreakTime = true;
      this.resultLine = undefined;
      // this.clearPriceElementsValue();
      this.updateOverUnderLabel(priceData, true);
      this.updateOverUnderIndicator(priceData, true);
      this.showResultValueOverlay(priceData);
      this.showOverUnderLabel(priceData);
      // this.disposeAllChart();
      this.activeResultLineIcon();
      this.deActiveBaseLineIcon();
      this.hideMainOverlay();
      // this.hideOverUnderIndicator();
      $('#main_chart_div').addClass('game_over');
      $('#main .row.bottom').addClass('game_over');
      // setTimeout(function() {
      //   this.showProgressImage();
      // }.bind(this), timeout);
    },

    clearPriceElementsValue: function() {
      this.clearCurrentPriceElem();
      this.clearBinancePriceElem();
      this.clearIdaxPriceElem();
    },

    clearCurrentPriceElem: function() {
      // var priceElem = document.querySelector('#main_overlay .current_price');
      // priceElem.querySelector('.value').textContent = '----.--';
      // this.removeAllUpAndDownClassName(priceElem);
    },

    clearBinancePriceElem: function() {
      // var priceElem = document.querySelector('#main_overlay .current_binance');
      // priceElem.querySelector('.value').textContent = '----.--';
    },

    clearIdaxPriceElem: function() {
      // var priceElem = document.querySelector('#main_overlay .current_digi');
      // priceElem.querySelector('.value').textContent = '----.--';
    },

    resetState: function() {
      this.count = -1;
      this.startPointBaseValue = 0;
      this.endPointBaseValue = 0;
    },

    hasNotMainChart: function() {
      return this.mainChart === undefined;
    },

    prepareMainChart: function(priceData) {
      this.mainChart = new App.MainChart('main_chart_div', priceData);
    },

    getMainChart: function() {
      return this.mainChart;
    },

    // prepareBaseChart: function(priceData) {
    //   this.baseChart = new App.UpDownChart('base_chart_div', priceData, 'left');
    // },

    // prepareResultChart: function(priceData) {
    //   this.resultChart = new App.UpDownChart('result_chart_div', priceData, 'right');
    // },

    getBasePoint: function() {
      return this.basePoint;
    },

    getEndPoint: function() {
      return this.endPoint;
    },

    startTimer: function() {
      if (!this.timeInterval) {
        this.timeInterval = setInterval(function() {
          document.querySelector('#main_overlay .current_time .value').textContent = moment().format('HH:mm:ss');
        }, 1000);
      }
    },

    stopTimer: function() {
      clearInterval(this.timeInterval);
    },

    isCloseRangeCount: function() {
      return this.count === 30;
    },

    isLastCount: function() {
      return this.count === this.endPoint;
    },

    isBetLimitAlertCount: function() {
      return this.count < 40 && this.count > 30;
    },

    isBetLimitOver: function() {
      return this.count >= 40;
    },

    isBasePointCount: function() {
      return this.count === this.basePoint;
    },

    isResultAreaCount: function() {
      return this.count > this.basePoint;
    },

    // addResultChart: function(priceData) {
    //   this.resultChart = new App.UpDownChart('result_chart_div', priceData, 'right');
    // },

    showServerCommunicationErrorOverlay: function() {
      $('#main_overlay').show();
      $('#main_overlay .mid.base_value_group .notification .title').addClass('blink');

      // $('#main_overlay .mid.base_value_group').show()
    },

    showBaseValueOverlay: function(priceData) {
      // this.showValueOverlay('#main_overlay .mid.base_value_group', priceData, 'open');
      $('#main .base_container .base_value_label .value').text(priceData.openAvg);
      $('#main .base_container .base_value_label').show();
      setTimeout(function() {
        $('#main .base_container .base_value_label').fadeOut('slow');
      }, 2000);
      // this.enableBaseValueOverlay();
    },

    showResultValueOverlay: function(priceData) {
      // this.showValueOverlay('#main_overlay .mid.result_value_group', priceData, 'result');
      // this.updateHighAndLowLabel(priceData);
      $('#main .result_container .result_value_label .value').text(priceData.resultAvg);
      $('#main .result_container .result_value_label').show();
      setTimeout(function() {
        $('#main .result_container .result_value_label').fadeOut('slow');
      }, 2000);
      this.disableBaseValueOverlay();
    },

    disableBaseValueOverlay: function() {
      $('#main .base_container .base_value_label').hide();
      // if (document.querySelector('#main_overlay .mid.base_value_group table .base .label').className.indexOf('disable') === -1) {
      //   document.querySelector('#main_overlay .mid.base_value_group table .base .label').className += ' disable';
      //   document.querySelector('#main_overlay .mid.base_value_group table .binance .label').className += ' disable';
      //   document.querySelector('#main_overlay .mid.base_value_group table .idax .label').className += ' disable';
      // }
    },

    enableBaseValueOverlay: function() {
      $('#main .base_container .base_value_label').show();
      // document.querySelector('#main_overlay .mid.base_value_group table .base .label').className =
      //   document.querySelector('#main_overlay .mid.base_value_group table .base .label').className.replace(' disable', '');
      //
      // document.querySelector('#main_overlay .mid.base_value_group table .binance .label').className =
      //   document.querySelector('#main_overlay .mid.base_value_group table .binance .label').className.replace(' disable', '');
      //
      // document.querySelector('#main_overlay .mid.base_value_group table .idax .label').className =
      //   document.querySelector('#main_overlay .mid.base_value_group table .idax .label').className.replace(' disable', '');
    },

    disableResultValueOverlay: function() {
      $('#main .base_container .result_value_label').hide();
      // if (document.querySelector('#main_overlay .mid.base_value_group table .base .label').className.indexOf('disable') === -1) {
      //   document.querySelector('#main_overlay .mid.base_value_group table .base .label').className += ' disable';
      //   document.querySelector('#main_overlay .mid.base_value_group table .binance .label').className += ' disable';
      //   document.querySelector('#main_overlay .mid.base_value_group table .idax .label').className += ' disable';
      // }
    },

    enableResultValueOverlay: function() {
      $('#main .base_container .result_value_label').show();
      // document.querySelector('#main_overlay .mid.base_value_group table .base .label').className =
      //   document.querySelector('#main_overlay .mid.base_value_group table .base .label').className.replace(' disable', '');
      //
      // document.querySelector('#main_overlay .mid.base_value_group table .binance .label').className =
      //   document.querySelector('#main_overlay .mid.base_value_group table .binance .label').className.replace(' disable', '');
      //
      // document.querySelector('#main_overlay .mid.base_value_group table .idax .label').className =
      //   document.querySelector('#main_overlay .mid.base_value_group table .idax .label').className.replace(' disable', '');
    },

    updateHighAndLowLabel: function(priceData) {
      var highAndLowLabel = 'HIGH';
      var highAndLowClassName = ' up';
      if (priceData.resultAvg === 0) {
        highAndLowLabel = '-';
      } else if (this.endPointBaseValue > priceData.resultAvg) {
        highAndLowLabel = 'LOW';
        highAndLowClassName = ' down';
      }
      var highAndLowElem = document.querySelector('#main_overlay .result_value_group .high_low_text');
      highAndLowElem.textContent = highAndLowLabel;
      highAndLowElem.className = highAndLowElem.className.replace(/\sup|\sdown/, '');
      highAndLowElem.className += highAndLowClassName;
    },

    showValueOverlay: function(elemSelector, priceData, prefix) {
      var avgKey = prefix + 'Avg';
      var biKey = prefix + 'Bi';
      var ixKey = prefix + 'Ix';
      var overlayElem = document.querySelector(elemSelector);
      if (overlayElem.style.display === '' || overlayElem.style.display === 'none') {
        if (priceData[biKey] === 0 || priceData[ixKey] === 0) {
          overlayElem.querySelector('.information').style.display = 'none';
          overlayElem.querySelector('.notification').style.display = 'block';
        } else {
          var binanceValueElem = overlayElem.querySelector('.information .binance .value');
          var idaxValueElem = overlayElem.querySelector('.information .digi .value');
          var resultValueElem = overlayElem.querySelector('.information .base .value');

          binanceValueElem.textContent = priceData[biKey].toFixed(2);
          idaxValueElem.textContent = priceData[ixKey].toFixed(2);
          resultValueElem.textContent = priceData[avgKey].toFixed(2);
          overlayElem.querySelector('.notification').style.display = 'none';
          overlayElem.querySelector('.information').style.display = 'block';
        }

        overlayElem.style.display = 'block';
      }
    },

    hideMainOverlay: function() {
      $('#main_overlay').hide();
      $('#main_overlay .mid.base_value_group .notification .title').removeClass('blink');
      // var midOverlays = document.querySelectorAll('#main_overlay .mid');
      // midOverlays[0].querySelector('table').style.display = 'none';
      // midOverlays[0].style.display = 'none';
      // midOverlays[1].querySelector('table').style.display = 'none';
      // midOverlays[1].style.display = 'none';
    },

    hideBreakTimeOverlay: function() {
      var breakTimeOverlay = document.querySelector('#break_time_overlay');
      breakTimeOverlay.style.display = 'none';
      var imageElem = breakTimeOverlay.querySelector('.image');
      imageElem.style.width = '100%';
      // var counterElem = breakTimeOverlay.querySelector('.label');
      // counterElem.textContent = '29초 후 시작합니다.';
    },

    disposeAllChart: function() {
      if (this.mainChart) {
        // this.mainChart.resetResultLine();
        // this.mainChart.getChart().dispose();
        am4core.disposeAllCharts();
      }

      // if (this.baseChart) {
      //   this.baseChart.getChart().dispose();
      // }

      // if (this.resultChart) {
      //   this.resultChart.getChart().dispose();
      // }
      this.mainChart = undefined;
      // this.baseChart = undefined;
      // this.resultChart = undefined;
    },

    showProgressImage: function(breakTimeLeft) {
      this.hideMainOverlay();
      this.disposeAllChart();

      var breakTimeOverlay = document.querySelector('#break_time_overlay');
      var imageElem = breakTimeOverlay.querySelector('.image');
      // var counterElem = breakTimeOverlay.querySelector('.label');
      if (breakTimeOverlay.style.display === '' || breakTimeOverlay.style.display === 'none' || imageElem.style.width === '0%') {
        imageElem.style.width = '100%';
        breakTimeOverlay.style.display = 'block';
        var count = 0;
        if (!_.isNil(breakTimeLeft)) {
          count = (30 - breakTimeLeft);
          imageElem.style.width = (100 - (3.4 * count)) + '%';
          // counterElem.textContent = (30 - count) + '초 후 시작합니다.';
        }
        var imageInterval = setInterval(function() {
          if (count === 30) {
            clearInterval(imageInterval);
            this.breakTime = false;
            return false;
          }
          var width = imageElem.style.width.replace('%', '');
          if (count < 29) {
            imageElem.style.width = Number(width) - 3.4 + '%';
          } else {
            imageElem.style.width = '0%';
          }
          // var counter = counterElem.textContent.replace('초 후 시작합니다.', '');
          // if (counter > 0) {
          // 	counter = (counter - 1) + '초 후 시작합니다.';
          // 	counterElem.textContent = counter;
          // }
          count++;
        }.bind(this), 1000);
      }
    },

    updateCurrentMinutesElem: function(priceData) {
      var startTime;
      var baseTime;
      var resultTime;
      var formatStr = 'HH:mm';
      var reqTime = moment(priceData.reqTime);
      if (priceData.count <= 60) {
        startTime = reqTime.format(formatStr);
        baseTime = reqTime.add(1, 'm').format(formatStr);
        resultTime = reqTime.add(1, 'm').format(formatStr);
      } else {
        startTime = reqTime.add(-1, 'm').format(formatStr);
        baseTime = reqTime.add(1, 'm').format(formatStr);
        resultTime = reqTime.add(1, 'm').format(formatStr);
      }

      document.querySelector('#main .bottom .start_time').textContent = startTime;
      document.querySelector('#main .bottom .base_time').textContent = baseTime;
      document.querySelector('#main .bottom .result_time').textContent = resultTime;
    },

    updateCurrentValueElem: function(priceData) {
      this.updateCurrentAverageElem(priceData);
      this.updateCurrentBinanceElem(priceData);
      this.updateCurrentIdaxElem(priceData);
    },

    updateCurrentAverageElem: function(priceData) {
      var value = priceData.avgPrice;
      var priceElem = document.querySelector('#main .current_avg');
      priceElem.querySelector('.value').textContent = value.toFixed(2);
    },
    updateOverUnderLabel: function(priceData, forceUpdate) {
      if (forceUpdate || (priceData.count > 60 && priceData.count <= 120)) {
        var value = priceData.avgPrice;
        var upAndDownClassName = '-';
        if (this.endPointBaseValue > 0) {
          if (this.endPointBaseValue > value) {
            upAndDownClassName = 'under';
          } else if (this.endPointBaseValue < value) {
            upAndDownClassName = 'over';
          }
        }

        this.removeAllUpAndDownClassName();

        var $resultLabelElem = $('#main .chart_container .result_label');
        $resultLabelElem.text(upAndDownClassName.toUpperCase());
        $resultLabelElem.addClass(upAndDownClassName);
      }
    },
    updateCurrentBinanceElem: function(priceData) {
      var value = priceData.biPrice;
      var priceElem = document.querySelector('#main .current_binance');
      priceElem.querySelector('.value').textContent = value.toFixed(2);
    },
    updateOverUnderIndicator: function(priceData, forceUpdate) {
      if (forceUpdate || (priceData.count > 60 && priceData.count <= 120)) {
        var value = priceData.avgPrice;
        var upAndDownClassName = '-';
        if (this.endPointBaseValue > 0) {
          if (this.endPointBaseValue > value) {
            upAndDownClassName = 'under';
          } else if (this.endPointBaseValue < value) {
            upAndDownClassName = 'over';
          }
        }

        this.removeAllUpAndDownIndicatorClassName();

        if (priceData.count < 11) {
          $('#main .chart_container .circle').addClass('disable');
        } else {
          var $circleElem = $('#main .chart_container .' + upAndDownClassName + '.circle');
          $circleElem.addClass('active');
          if (upAndDownClassName === 'under') {
            $('#main .chart_container .over.circle').addClass('disable');
            $('#main .chart_container .under.circle').addClass('positive');
          } else {
            $('#main .chart_container .under.circle').addClass('disable');
            $('#main .chart_container .over.circle').addClass('positive');
          }
        }
      }
    },

    updateCurrentIdaxElem: function(priceData) {
      var value = priceData.ixPrice;
      var priceElem = document.querySelector('#main .current_digi');
      priceElem.querySelector('.value').textContent = value.toFixed(2);
    },

    removeAllUpAndDownClassName: function() {
      var $resultLabelElem = $('#main .chart_container .result_label');
      $resultLabelElem.removeClass('-');
      $resultLabelElem.removeClass('over');
      $resultLabelElem.removeClass('under');
    },

    removeAllUpAndDownIndicatorClassName: function() {
      var $circleElem = $('#main .chart_container .circle');
      $circleElem.removeClass('active');
      $circleElem.removeClass('disable');
      $circleElem.removeClass('negative');
      $circleElem.removeClass('positive');
    },

    updateBaseLineFlag: function(priceData) {
      document.querySelector('#main .bottom .base_flag').className =
        document.querySelector('#main .bottom .base_flag').className.replace(' enable', '');
      if (priceData.count >= this.basePoint || priceData.count < 11) {
        document.querySelector('#main .bottom .base_flag').className += ' enable';
      }
    },

    updateBetLimitLineFlag: function(priceData) {
      document.querySelector('#main .bottom .bet_limit_flag').className =
        document.querySelector('#main .bottom .bet_limit_flag').className.replace(' enable', '');
      if (priceData.count >= 40) {
        document.querySelector('#main .bottom .bet_limit_flag').className += ' enable';
      }
    },

    updateResultLineFlag: function(priceData) {
      document.querySelector('#main .bottom .result_flag').className =
        document.querySelector('#main .bottom .result_flag').className.replace(' enable', '');
      if (priceData.count >= 120 || priceData.count < 11) {
        document.querySelector('#main .bottom .result_flag').className += ' enable';
      }
    },


    activeBetLimitIcon: function() {
      $('#main .bottom .bet_limit.circle').addClass('active');
    },

    disableBetLimitIcon: function() {
      $('#main .bottom .bet_limit.circle').removeClass('active');
      $('#main .bottom .bet_limit.circle.bg').addClass('disable');
    },
    activeBaseLineIcon: function() {
      $('#main .bottom .base.circle').addClass('active');
      $('#main .bottom .base.circle.bg').addClass('enable');
    },
    activeResultLineIcon: function() {
      $('#main .bottom .result.circle').addClass('active');
      $('#main .bottom .result.circle.bg').addClass('enable');
    },
    deActiveBaseLineIcon: function() {
      $('#main .bottom .base.circle').removeClass('active');
      $('#main .bottom .base.circle.bg').removeClass('enable');
    },
    hideOverUnderIndicator: function() {
      $('#main .chart_container .top_overlay').hide();
    },

    // updateSideChartMinMaxValue: function() {
    //   this.baseChart.updateMaxValue(this.mainChart.getMaxValue());
    //   this.baseChart.updateMinValue(this.mainChart.getMinValue());
    //   if (this.resultChart) {
    //     this.resultChart.updateMaxValue(this.mainChart.getMaxValue());
    //     this.resultChart.updateMinValue(this.mainChart.getMinValue());
    //   }
    // },
  };
}());
