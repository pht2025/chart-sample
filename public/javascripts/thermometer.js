App.thermometer = (function () {
  return {
    svgDoc: undefined,
    leftChart: undefined,
    rightChart: undefined,
    leftContainer: undefined,
    rightContainer: undefined,
    leftText: undefined,
    leftBeginText: undefined,
    leftLastText: undefined,
    leftLine: undefined,
    leftLineText: undefined,
    leftBar: undefined,

    rightBeginText: undefined,
    rightLastText: undefined,
    rightText: undefined,
    rightLine: undefined,
    rightLineText: undefined,
    rightBar: undefined,

    betAvailableSuffix: '',
    betResultReadySuffix: '',

    animationDuration: 1000,

    winTextColor: '#5D98F9',
    winBarColor: '#346DE0',
    loseTextColor: '#F73E4C',
    loseBarColor: '#C42A26',

    loseTextIcon: 'snow',
    winTextIcon: 'sun',

    timelineStepData: {
      '1': 0,
      '4': 59.7, '6': 75.4, '8': 91.1,
      '10': 106, '12': 149,
      '14': 164.5, '16': 180, '18': 195.5, '20': 211, '22': 226.5, '24': 242, '26': 257.5,
      '28': 273, '30': 288.5, '32': 304, '34': 319.5, '36': 335, '38': 350.5,
      '40': 366, '42': 409,
      '44': 422.9, '46': 436.8, '48': 450.7,
      '50': 464.6, '52': 478.5, '54': 492.4, '56': 506.3,
      '58': 520, '60': 564,
    },
    timelineTextStepData: {
      '1': 0,
      '4': 35.7, '6': 51.4, '8': 67.1,
      '10': 105, '12': 125,
      '14': 140.5, '16': 156, '18': 171.5, '20': 187, '22': 202.5, '24': 218, '26': 233.5,
      '28': 249, '30': 264.5, '32': 280, '34': 295.5, '36': 311, '38': 326.5,
      '40': 365, '42': 385,
      '44': 398.9, '46': 412.8, '48': 426.7,
      '50': 440.6, '52': 454.5, '54': 468.4, '56': 482.3,
      '58': 500, '60': 520,
    },
    timelineStep: [15.7, 15.5, 13.9],

    cleanUpCompleted: false,
    betCloseAlertTimer: undefined,

    leftChartData: [
      { 'name': 'B', 'value': 40, 'disabled': true },
      { 'name': 'A', 'value': 60 },
    ],
    rightChartData: [
      { 'name': 'B', 'value': 40, 'disabled': true },
      { 'name': 'A', 'value': 60 },
    ],
    leftBarPathArrayValue: [
      ['M', 145.6, 498.9],
      ['H', 12.6],
      ['C', 6, 498.9, 0.6, 490.6, 0.6, 480.4],
      ['V', 86.4],
      ['C', 0.6, 76.2, 6, 67.9, 12.6, 67.9],
      ['H', 145.6],
      ['C', 152.2, 67.9, 157.6, 76.2, 157.6, 86.4],
      ['V', 480.4],
      ['C', 157.6, 490.6, 152.2, 498.9, 145.6, 498.9],
      ['Z'],
    ],
    rightBarPathArrayValue: [
      ['M', 202.2, 498.9],
      ['H', 69.2],
      ['C', 62.6, 498.9, 57.2, 490.6, 57.2, 480.4],
      ['V', 85.4],
      ['C', 57.2, 75.2, 62.6, 66.9, 69.2, 66.9],
      ['H', 202.2],
      ['C', 208.8, 66.9, 214.2, 75.2, 214.2, 85.4],
      ['V', 480.3],
      ['C', 214.2, 490.6, 208.8, 498.9, 202.2, 498.9],
      ['Z'],
    ],
    start: function () {
      var timelineElem = document.querySelector('#thermometer')
      this.svgDoc = timelineElem.contentDocument
      var socket = io.connect(location.origin)
      socket.on('priceData', this.dataReceivedHandler.bind(this))
    },
    dataReceivedHandler: function (priceData) {
      // console.log('price data', priceData);
      this.updateTimelineState(priceData)

      if (priceData.count < 11) {
        return
      }

      this.cleanUp()

      if (priceData.count >= 30 && priceData.count < 40) {
        this.alertBetCloseTime()
      }
      if (priceData.count >= 40) {
        if (this.betCloseAlertTimer) {
          clearInterval(this.betCloseAlertTimer)
          this.betCloseAlertTimer = undefined
          $('#chart-wrap').removeClass('blink')
        }
      }
      this.updateBarState(priceData)
      this.updateMainValue(priceData)
      this.updateAvgValue(priceData)
      this.updateBarTextGroup(priceData)

      if (priceData.count === 60) {
        this.gameOver(priceData)
      }
    },
    alertBetCloseTime: function () {
      if (!this.betCloseAlertTimer) {
        this.betCloseAlertTimer = setInterval(function () {
          $('#chart-wrap').toggleClass('blink')
        }, 650)
      }
    },
    gameOver: function (priceData) {
      this.cleanUpCompleted = false
      var $leftGroup = $(this.svgDoc.querySelector('#left'))
      var $rightGroup = $(this.svgDoc.querySelector('#right'))
      var $leftBg = $(this.svgDoc.querySelector('#left_bg'))
      var $rightBg = $(this.svgDoc.querySelector('#right_bg'))

      var winner = this.whoIsWinner(priceData)
      if (winner === 'bi') {
        $leftBg.addClass('l_winner')
        $rightGroup.addClass('loser')
      } else if (winner === 'ix') {
        $leftGroup.addClass('loser')
        $rightBg.addClass('r_winner')
      } else {
        $leftBg.addClass('l_winner')
        $rightBg.addClass('r_winner')
      }
    },
    cleanUp: function () {
      if (this.cleanUpCompleted) {
        return
      }
      this.cleanUpCompleted = true
      var $leftGroup = $(this.svgDoc.querySelector('#left'))
      var $rightGroup = $(this.svgDoc.querySelector('#right'))
      var $leftBg = $(this.svgDoc.querySelector('#left_bg'))
      var $rightBg = $(this.svgDoc.querySelector('#right_bg'))
      $leftGroup.removeClass('loser')
      $rightGroup.removeClass('loser')
      $leftBg.removeClass('l_winner')
      $rightBg.removeClass('r_winner')
    },
    whoIsWinner: function (priceData) {
      var biLastValue = Number(this.getBiLastValue(priceData))
      var ixLastValue = Number(this.getIxLastValue(priceData))
      if (biLastValue > ixLastValue) {
        return 'bi'
      } else if (biLastValue < ixLastValue) {
        return 'ix'
      }

      return null
    },
    updateTimelineState: function (priceData) {
      this.updateTimelineBar(priceData)
      this.updateTimelineText(priceData)
    },
    updateTimelineBar: function (priceData) {
      var $timelineBoxElem = $(this.svgDoc.querySelector('#mask_x5F_box'))
      var $timelineTextGroup = $(this.svgDoc.querySelector('#timeline_text_group'))

      if (priceData.count > 1 && !$timelineBoxElem.hasClass('animation')) {
        $(this.svgDoc.querySelector('#gradation_x5F_line .st3')).show()
        $timelineBoxElem.addClass('animation')
        $timelineTextGroup.addClass('animation')
      }

      if (priceData.count === 1) {
        $timelineTextGroup.css('transform', 'translate(0, ' + this.timelineTextStepData[priceData.count] + 'px)')
        $timelineBoxElem.attr('height', this.timelineStepData[priceData.count])
      } else {
        if (this.timelineTextStepData[priceData.count + 2]) {
          $timelineTextGroup.css('transform', 'translate(0, ' + this.timelineTextStepData[priceData.count + 2] + 'px)')
          $timelineBoxElem.attr('height', this.timelineStepData[priceData.count + 2])
        }
      }

      if (priceData.count >= 41) {
        $(this.svgDoc.querySelector('#gradation_x5F_line .st3')).hide()
      }

      if (priceData.count === 60) {
        $timelineBoxElem.removeClass('animation')
        $timelineTextGroup.removeClass('animation')
      }
    },
    updateTimelineText: function (priceData) {
      var $timelineBoxElem = $(this.svgDoc.querySelector('#timeline_text_group'))
      var $timelineText = $timelineBoxElem.find('text')
      $timelineText.text(priceData.count)
    },
    updateBarState: function (priceData) {
      var $leftBarElem = $(this.svgDoc.querySelector('#graph_x5F_redColor'))
      var $rightBarElem = $(this.svgDoc.querySelector('#graph_x5F_blueColor'))
      var leftBarDAttr = $leftBarElem.attr('d')
      var rightBarDAttr = $rightBarElem.attr('d')
      var leftBarList = leftBarDAttr.split(/(?=[MmLlHhVvCcSsQqTtAaZz])/)
      var rightBarList = rightBarDAttr.split(/(?=[MmLlHhVvCcSsQqTtAaZz])/)
      leftBarList[3] = 'V' + this.makeLeftYPosition(this.getBiLastValue(priceData))
      rightBarList[3] = 'V' + this.makeRightYPosition(this.getIxLastValue(priceData))
      $leftBarElem.attr('d', leftBarList.join(''))
      $rightBarElem.attr('d', rightBarList.join(''))

      var barColors = this.getBarColors(priceData)
      $leftBarElem.attr('fill', barColors.bi)
      $rightBarElem.attr('fill', barColors.ix)
    },
    updateBarText: function (priceData) {
      var $leftBarText = $(this.svgDoc.querySelector('#R_x5F_sun_x5F_snow text'))
      var $rightBarText = $(this.svgDoc.querySelector('#B_x5F_sun_x5F_snow text'))
      $leftBarText.text(this.getBiLastValue(priceData))
      $rightBarText.text(this.getIxLastValue(priceData))

      var textColors = this.getValueTextColors(priceData)
      $leftBarText.attr('fill', textColors.bi)
      $rightBarText.attr('fill', textColors.ix)
    },
    updateMainValue: function (priceData) {
      var $leftText = $(this.svgDoc.querySelector('#left_text'))
      var $rightText = $(this.svgDoc.querySelector('#right_text'))

      $leftText.find('.first').text(this.getBiFirstValue(priceData) + '.')
      $rightText.find('.first').text(this.getIxFirstValue(priceData) + '.')

      $leftText.find('.last').text(this.getBiLastValue(priceData))
      $rightText.find('.last').text(this.getIxLastValue(priceData))

      var textColors = this.getValueTextColors(priceData)
      $leftText.find('.last').attr('fill', textColors.bi)
      $rightText.find('.last').attr('fill', textColors.ix)
    },
    updateAvgValue: function (priceData) {
      var $centerText = $(this.svgDoc.querySelector('#center_text'))
      $centerText.find('.first').text(this.getAvgFirstValue(priceData) + '.')
      $centerText.find('.last').text(this.getAvgLastValue(priceData))
    },
    updateBarTextGroup: function (priceData) {
      var leftYPosition = this.makeLineYPosition(this.getBiLastValue(priceData))
      var rightYPosition = this.makeLineYPosition(this.getIxLastValue(priceData))
      this.updateBarText(priceData)
      this.toggleBarTextGroupIcon(priceData)
      this.updateBarTextGroupPosition(leftYPosition, rightYPosition)
    },
    updateBarTextGroupPosition: function (leftYPosition, rightYPosition) {
      var $leftBarTextGroup = $(this.svgDoc.querySelector('#R_x5F_sun_x5F_snow'))
      var $rightBarTextGroup = $(this.svgDoc.querySelector('#B_x5F_sun_x5F_snow'))
      $leftBarTextGroup.css('transform', 'translate(0, ' + leftYPosition + 'px)')
      $rightBarTextGroup.css('transform', 'translate(0, ' + rightYPosition + 'px)')
    },
    toggleBarTextGroupIcon: function (priceData) {
      var $leftBarTextGroup = $(this.svgDoc.querySelector('#R_x5F_sun_x5F_snow'))
      var $rightBarTextGroup = $(this.svgDoc.querySelector('#B_x5F_sun_x5F_snow'))

      $leftBarTextGroup.removeClass('snow').removeClass('sun')
      $rightBarTextGroup.removeClass('snow').removeClass('sun')

      var barIcons = this.getBarTextIcons(priceData)
      $leftBarTextGroup.addClass(barIcons.bi)
      $rightBarTextGroup.addClass(barIcons.ix)
    },
    getBiFirstValue: function (priceData) {
      var value = priceData.binance.toString()
      return value.split('.')[0]
    },
    getIxFirstValue: function (priceData) {
      var value = priceData.idax.toString()
      return value.split('.')[0]
    },
    getBiLastValue: function (priceData) {
      var value = priceData.binance.toString()
      var sepValue = value.split('.')
      return sepValue[1] ? (sepValue[1].length < 2 ? sepValue[1] + '0' : sepValue[1]) : '00'
    },
    getIxLastValue: function (priceData) {
      var value = priceData.idax.toString()
      var sepValue = value.split('.')
      return sepValue[1] ? (sepValue[1].length < 2 ? sepValue[1] + '0' : sepValue[1]) : '00'
    },
    getAvgFirstValue: function (priceData) {
      var value = priceData.avgPrice.toString()
      return value.split('.')[0]
    },
    getAvgLastValue: function (priceData) {
      var value = priceData.avgPrice.toString()
      var sepValue = value.split('.')
      return sepValue[1] ? (sepValue[1].length < 2 ? sepValue[1] + '0' : sepValue[1]) : '00'
    },
    getBarColors: function (priceData) {
      var biString = priceData.binance.toString()
      var ixString = priceData.idax.toString()

      var sepBiValue = biString.split('.')
      var sepIxValue = ixString.split('.')
      var biLastValue = sepBiValue[1] ? (sepBiValue[1].length < 2 ? sepBiValue[1] + '0' : sepBiValue[1]) : '00'
      var ixLastValue = sepIxValue[1] ? (sepIxValue[1].length < 2 ? sepIxValue[1] + '0' : sepIxValue[1]) : '00'

      var leftBarColor = ''
      var rightBarColor = ''

      if (biLastValue > ixLastValue) {
        leftBarColor = this.loseBarColor
        rightBarColor = this.winBarColor
      } else {
        leftBarColor = this.winBarColor
        rightBarColor = this.loseBarColor
      }
      return {
        bi: leftBarColor, ix: rightBarColor,
      }
    },
    getValueTextColors: function (priceData) {
      var biString = priceData.binance.toString()
      var ixString = priceData.idax.toString()

      var sepBiValue = biString.split('.')
      var sepIxValue = ixString.split('.')
      var biLastValue = sepBiValue[1] ? (sepBiValue[1].length < 2 ? sepBiValue[1] + '0' : sepBiValue[1]) : '00'
      var ixLastValue = sepIxValue[1] ? (sepIxValue[1].length < 2 ? sepIxValue[1] + '0' : sepIxValue[1]) : '00'

      var leftTextColor = ''
      var rightTextColor = ''

      if (biLastValue > ixLastValue) {
        leftTextColor = this.loseTextColor
        rightTextColor = this.winTextColor
      } else {
        leftTextColor = this.winTextColor
        rightTextColor = this.loseTextColor
      }
      return {
        bi: leftTextColor, ix: rightTextColor,
      }
    },
    getBarTextIcons: function (priceData) {
      var biString = priceData.binance.toString()
      var ixString = priceData.idax.toString()

      var sepBiValue = biString.split('.')
      var sepIxValue = ixString.split('.')
      var biLastValue = sepBiValue[1] ? (sepBiValue[1].length < 2 ? sepBiValue[1] + '0' : sepBiValue[1]) : '00'
      var ixLastValue = sepIxValue[1] ? (sepIxValue[1].length < 2 ? sepIxValue[1] + '0' : sepIxValue[1]) : '00'

      var leftTextIcon = 'sun'
      var rightTextIcon = 'snow'

      if (biLastValue > ixLastValue) {
        leftTextIcon = this.winTextIcon
        rightTextIcon = this.loseTextIcon
      } else {
        leftTextIcon = this.loseTextIcon
        rightTextIcon = this.winTextIcon
      }
      return {
        bi: leftTextIcon, ix: rightTextIcon,
      }
    },
    makeLeftYPosition: function (value) {
      return (536 - (value * 4.16))
    },
    makeRightYPosition: function (value) {
      return (530 - (value * 4.14))
    },
    makeLineYPosition: function (value) {
      return (416 - (value * 4.16))
    },
  }
}())
