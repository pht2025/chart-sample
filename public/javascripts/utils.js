App.utils = {
  getLastValue: function (valueArray) {
    if (_.isArray(valueArray)) {
      return valueArray[valueArray.length - 1]
    }
    return 0
  },
  isQA: function () {
    return true//location.hash.toLowerCase() === '#qa';
  },
  isThermometer: function () {
    return location.hash.toLowerCase() === '#thermometer'
  },
  getMaxValue: function (priceData) {
    if (priceData.binance === priceData.idax) {
      return Number(priceData.binance) + 0.5
    }
    return priceData.binance > priceData.idax ? priceData.binance : priceData.idax
  },
  getMinValue: function (priceData) {
    if (priceData.binance === priceData.idax) {
      return Number(priceData.binance) - 0.5
    }
    return priceData.binance > priceData.idax ? priceData.idax : priceData.binance
  },
}
