const axios = require('axios')
const util = require('util')
const request = require('request')
const log4js = require('log4js')
const logger = log4js.getLogger('crawler')
const moment = require('moment')
const BIN_PROC = {
  NONE: 0,
  START: 1,
  OPEN: 2,
  RESULT: 3,
}
const API_GET_TIME_OUT = 500
const KLINE_API_GET_TIME_OUT = 1000 * 7
const IDAX_OK = 10000
const DIGI_FINEX_OK = 0

const url = 'https://openapi.idax.pro/api/v2'
const idaxBaseUrl = 'https://openapi.idax.pro/api/v2/kline?pair=BTC_USDT&period=1min&size=1&since=' //&size= , &since값 필요 , since값을 필요한 시간 지난후 호출
const binanceBaseUrl = 'https://api.binance.com/api/v1/klines?symbol=BTCUSDT&interval=1m&limit=1&startTime=' //&startTime = , &limit=1 필요
const binanceTimeBaseUrl = 'https://api.binance.com/api/v1/klines?symbol=BTCUSDT&interval=1m&limit=1' //&startTime = , &limit=1 필요
const idaxTimeBaseUrl = 'https://openapi.idax.pro/api/v2/kline?pair=BTC_USDT&period=1min&size=1' //&startTime = , &limit=1 필요
const idaxTime = 'https://openapi.idax.pro/api/v2/time' //{"code":10000,"msg":"Successful request processing","timestamp":1554950685516}
const binanceTime = 'https://api.binance.com/api/v1/time' //{"serverTime":1554950689933}
const digiFinexTime = 'https://openapi.digifinex.com/v3/time' //{"serverTime":1554950689933}

let serverTimes = null
let diff = 0
let startBaseTime = 0
const delayTime = 2 * 1000 // 2초간 딜레이(idax 및 binance 서버에서 분봉(kline) 데이터를 완성하기까지 예상 딜레이 시간 만큼 추가).
const oneMinutes = 60 * 1000
const beforeCrawling = async function () {
  serverTimes = await getServerTime()
  let nowTemp = new Date()

  //let selectTime = (serverTimes.bnTime > serverTimes.idTime) ? serverTimes.bnTime : serverTimes.idTime;
  let selectTime = (serverTimes.bnTime > serverTimes.idTime) ? serverTimes.idTime : serverTimes.bnTime //더 작은값이 느린시간?? 이겠지

  if (startBaseTime === 0) {
    startBaseTime = await getStartBaseTime()
  }
  diff = (new Date(selectTime)).getTime() - nowTemp.getTime()
}

const priceData = {
  state: BIN_PROC.NONE, count: 0, reqTime: 0,
  av: 0, binance: 0, idax: 0,
  openAvg: 0, openBi: 0, openIx: 0,
  resultAvg: 0, resultBi: 0, resultIx: 0,
  resultBaseValue: 0,
}

const getPriceData = function () {
  return priceData
}

let crawlerCounter = 0
const increaseCount = function () {
  const now = new Date()
  const min = now.getMinutes()
  const sec = now.getSeconds()
  let count = sec - 1
  if (min % 2 === 0) {
    if (sec === 0) {
      count = 118
    } else if (sec === 1) {
      count = 119
    } else if (sec === 2) {
      count = 120
    } else {
      count = sec - 2
    }
  } else if (min % 2 === 1) {
    if (sec === 0) {
      count = 58
    } else if (sec === 1) {
      count = 59
    } else if (sec === 2) {
      count = 60
    } else {
      count = (sec - 2) + 60
    }
  }
  // if (sec === 0) {
  //   count = 56;
  // } else if (sec === 1) {
  //   count = 57;
  // } else if (sec === 2) {
  //   count = 58;
  // } else if (sec === 3) {
  //   count = 59;
  // } else if (sec === 4) {
  //   count = 60;
  // }

  if (count < 11) {
    startBaseTime = getStartBaseTime()
  }

  crawlerCounter = count
  logger.info('Crawler counter increase done : ', crawlerCounter)
}

function resetPriceData(crawlerCounter = 0) {
  priceData.state = BIN_PROC.NONE
  priceData.count = crawlerCounter
  // priceData.openAvg = 0;
  // priceData.openBi = 0;
  // priceData.openIx = 0;
  // priceData.resultAvg = 0;
  // priceData.resultBi = 0;
  // priceData.resultIx = 0;
  priceData.avgPrice = 0
  priceData.biPrice = 0
  priceData.ixPrice = 0
  priceData.reqTime = 0
  priceData.startBaseValue = 0

  priceData.idax = 0
  priceData.binance = 0
  priceData.av = 0
}

function resetOpenPriceData() {
  priceData.openAvg = 0
  priceData.openBi = 0
  priceData.openIx = 0
  priceData.resultBaseValue = 0
}

function resetResultPriceData() {
  priceData.resultAvg = 0
  priceData.resultBi = 0
  priceData.resultIx = 0
}

function setStartData() {
  priceData.state = BIN_PROC.START
}

function setOpenData() {
  priceData.state = BIN_PROC.OPEN
}

function setResultData() {
  priceData.state = BIN_PROC.RESULT
}

let klineDataStatus = true
let prevData
let basePrintingDone = false
let resultPrintingDone = false
let getAllKlineValueDone = false
let getResultValueDone = false
let iMinDelayTime = 0
let iMaxDelayTime = 0
let iLastDelayTime = 0
let dMinDelayTime = 0
let dMaxDelayTime = 0
let dLastDelayTime = 0
let digifinexDone = false
let biDone = false
let klineErrorCount = 0
let iEightCount = 0
let dEightCount = 0
let iOneMinutesCount = 0
let dOneMinutesCount = 0
let getBaseKlineSuccess = false
let getResultKlineSuccess = false
const startCrawling = async function (baseTime) {
  increaseCount()
  logger.info('Start crawling : ', crawlerCounter)
  logger.info('Start time : ', new Date())
  resetPriceData(crawlerCounter)
  let data = undefined

  if (getBaseKlineSuccess && (crawlerCounter > 11 && crawlerCounter < 60)) {
    getBaseKlineSuccess = false
  }

  if (getResultKlineSuccess && (crawlerCounter > 60)) {
    getResultKlineSuccess = false
  }

  if (crawlerCounter < 11) {
    resetOpenPriceData()
  } else if (crawlerCounter >= 60) {
    resetResultPriceData()
  }

  if (crawlerCounter > 60 && (!biDone || !digifinexDone)) {
    if (!biDone) {
      logger.info(`### [${crawlerCounter}] BASE binance still running.....`)
    }
    if (!digifinexDone) {
      logger.info(`### [${crawlerCounter}] BASE digifinex sill running.....`)
    }
    return
  }

  if (crawlerCounter < 11 && (!biDone || !digifinexDone)) {
    if (!biDone) {
      logger.info(`### [${crawlerCounter}] RESULT binance still running.....`)
    }
    if (!digifinexDone) {
      logger.info(`### [${crawlerCounter}] RESULT digifinex sill running.....`)
    }
    return
  }

  if (!basePrintingDone && crawlerCounter > 60 && (biDone && digifinexDone)) {
    printTimeCheckSummary()
    basePrintingDone = true
    resultPrintingDone = false
  }

  if (!resultPrintingDone && crawlerCounter < 11 && (biDone && digifinexDone)) {
    printTimeCheckSummary()
    basePrintingDone = false
    resultPrintingDone = true
  }

  const reqTime = startBaseTime//Number(moment().add(-1, 'm').format('x'));
  if (crawlerCounter === 0) { // 최초 실행시
    try {
      data = await getAllKline(reqTime)
    } catch (e) {
      logger.error('getAllKline error COUNT : ', crawlerCounter, ' reqTime:', reqTime)
      logger.error('getAllKline error', e)
    }
  } else if (crawlerCounter === 60 && !getAllKlineValueDone) {
    try {
      data = await getAllKline(reqTime)
      if (data.binance > 0 && data.idax > 0 && crawlerCounter <= 67) {
        getBaseKlineSuccess = true
      } else {
        getBaseKlineSuccess = false
      }
      getAllKlineValueDone = true
    } catch (e) {
      logger.error('getAllKline error COUNT : ', crawlerCounter, ' reqTime:', reqTime)
      logger.error('getAllKline error', e)
    }
    startBaseTime += oneMinutes
  } else if (getBaseKlineSuccess && crawlerCounter === 120 && !getAllKlineValueDone) {
    try {
      data = await getAllKline(reqTime)
      if (data.binance > 0 && data.idax > 0 && (crawlerCounter === 120 || crawlerCounter <= 7)) {
        getResultKlineSuccess = true
      } else {
        getResultKlineSuccess = false
      }
      getAllKlineValueDone = true
    } catch (e) {
      logger.error('getAllKline error COUNT : ', crawlerCounter, ' reqTime:', reqTime)
      logger.error('getAllKline error', e)
    }
    startBaseTime += oneMinutes
  } else {
    getAllKlineValueDone = false
    try {
      data = await getAllPrice()
      // data = await getAllKline(reqTime);
    } catch (e) {
      logger.error('getAllPrice error COUNT : ', crawlerCounter, ' reqTime:', reqTime)
      logger.error('getAllPrice error', e)
    }
  }

  if (data) {
    prevData = data
  } else {
    if (crawlerCounter % 60 === 0) {
      data = { av: 0, binance: 0, idax: 0 }
    } else {
      data = prevData
    }
  }

  if (crawlerCounter === 1 && (data.binance === 0 || data.idax === 0)) {
    if (data.idax > 0) {
      data.binance = data.idax + 0.5
    } else {
      data.idax = data.binance + 0.5
    }
  }

  priceData.avgPrice = data.av
  priceData.biPrice = data.binance
  priceData.ixPrice = data.idax
  priceData.reqTime = reqTime
  priceData.av = data.av
  priceData.binance = data.binance
  priceData.idax = data.idax
  priceData.count = crawlerCounter

  if ((priceData.count < 120 && priceData.count >= 60) && (biDone && digifinexDone)) {
    logger.info('++++++++++++++++++ getBaseKlineSuccess', getBaseKlineSuccess)
    logger.info('++++++++++++++++++ data', JSON.stringify(data))
    if (!getBaseKlineSuccess || (data.binance <= 0 || data.idax <= 0)) {
      priceData.idax = 0
      priceData.binance = 0
      priceData.av = 0

      priceData.openAvg = 0
      priceData.openBi = 0
      priceData.openIx = 0
      priceData.resultBaseValue = 0
    } else {
      if (priceData.count > 60 && priceData.openAvg === 0) {
        priceData.count = 60
      }
      if (priceData.openAvg === 0) {
        priceData.openAvg = data.av
        priceData.openBi = data.binance
        priceData.openIx = data.idax
        priceData.resultBaseValue = data.openAvg
      }
    }
  } else if ((priceData.count === 120 || priceData.count < 11) && (biDone && digifinexDone)) {
    if (!getResultKlineSuccess || (data.binance <= 0 || data.idax <= 0)) {
      priceData.idax = 0
      priceData.binance = 0
      priceData.av = 0

      priceData.resultAvg = 0
      priceData.resultBi = 0
      priceData.resultIx = 0
      klineDataStatus = false
    } else {
      priceData.resultAvg = data.av
      priceData.resultBi = data.binance
      priceData.resultIx = data.idax
    }
    priceData.resultTime = reqTime
  }

  if (crawlerCounter > 10 && crawlerCounter < 60) {
    setStartData(data)
  } else if (crawlerCounter >= 60 && crawlerCounter < 120) {
    setOpenData(data)
  } else if (crawlerCounter === 120 || crawlerCounter <= 10) {
    setResultData(data)
  }

  logger.info('Start crawling end : ', JSON.stringify(priceData))
  // console.log('base time', baseTime, new Date(baseTime));
  // const now = new Date();
  // let changedTimetable = false;
  // if (startBaseTime + delayTime + oneMinutes < (now - 1000)) { // 2초후 다음 1분봉 데이터 요청을 위해 계산
  // 	startBaseTime += oneMinutes;
  //
  // 	const baseMinutes = new Date(startBaseTime).getMinutes();
  // 	logger.info('base minutes', baseMinutes, startBaseTime, ',baseTime:', new Date(startBaseTime).toString(), ',local time:', new Date().toString());
  // 	// if (baseMinutes === 0 || (baseMinutes % 3 === 0)) { // 시작 (매시 00분, 03분, 06분, 3의 배수)
  // 	if (baseMinutes === 0 || (baseMinutes % 2 === 0)) { // 시작 (매시 00분, 02분, 04분, 2의 배수)
  // 		changedTimetable = true;
  // 	}
  // }
  //
  // if (changedTimetable) {
  // 	logger.info('reset price data', priceData.count);
  // 	resetPriceData();
  // }
  //
  // priceData.count++;
  //
  // console.log('startBaseTime', startBaseTime, now.getTime());
  //
  // let data;
  // if (priceData.count === 59 || priceData.count === 119) {
  // 	data = await getAllKline(startBaseTime);
  // } else {
  // 	data = await getAllPrice();
  // }
  //
  // if (priceData.count === 0) {
  // 	priceData.startTime = startBaseTime;
  // 	priceData.startBaseValue = data.av;
  // }
  //
  // if (priceData.count === 59) {
  // 	priceData.openAvg = data.av;
  // 	priceData.openBi = data.binance;
  // 	priceData.openIx = data.idax;
  // 	priceData.resultBaseValue = data.openAvg;
  // } else if (priceData.count === 119) {
  // 	priceData.resultAvg = data.av;
  // 	priceData.resultBi = data.binance;
  // 	priceData.resultIx = data.idax;
  // 	priceData.resultTime = startBaseTime;
  // }
  //
  // priceData.avgPrice = data.av;
  // priceData.biPrice = data.binance;
  // priceData.ixPrice = data.idax;
  // priceData.reqTime = startBaseTime;
  // priceData.av = data.av;
  // priceData.binance = data.binance;
  // priceData.idax = data.idax;
  //
  // const baseMinutes = new Date(startBaseTime).getMinutes();
  // logger.info('changed timetable', changedTimetable, ', baseM', baseMinutes, ' : ', baseMinutes % 3);
  // if (baseMinutes === 0) { // 시작 (매시 00분)
  // 	setStartData(data);
  // } else if (baseMinutes === 1) { // 기준
  // 	setOpenData(data);
  // } else if (baseMinutes === 2) { // 마감
  // 	setResultData(data);
  // } else {
  // 	switch (baseMinutes % 2) {
  // 		case 0: // 시작
  // 			setStartData(data);
  // 			break;
  // 		case 1: // 기준
  // 			setOpenData(data);
  // 			break;
  // 		case 2: // 마감
  // 			setResultData(data);
  // 			break;
  // 	}
  // }
  // logger.info('Price Data: state(%s), time(%s), count(%s), av(%s), bi(%s), ix(%s)',
  // 	priceData.state, priceData.reqTime, priceData.count, priceData.av, priceData.binance, priceData.idax);
}

const getAllPrice = async function () {
  logger.info('====================== getAllPrice ==========================')
  const p1 = axios.get('https://api.binance.com/api/v3/ticker/price?symbol=BTCUSDT', { timeout: API_GET_TIME_OUT });
  const p2 = axios.get('https://openapi.digifinex.vip/v3/trades?symbol=btc_usdt&limit=1', { timeout: API_GET_TIME_OUT })
  const cTimestamp = new Date().getTime()

  return Promise.all([p1, p2]).then(function (results) {
    const p1Result = results[0]
    const p2Result = results[1]
    if (p2Result.data.code === 0) {
      const binance = {
        code: 10000,
        msg: 'Successful request processing',
        kline: [[
          cTimestamp,
          p1Result.data.price,
          p1Result.data.price,
          p1Result.data.price,
          p1Result.data.price,
          '39.031123',
        ]],
      }
      const digi = {
        code: p2Result.data.code,
        data: [[
          cTimestamp, 18.2448,
          p2Result.data.data[0].price,
          11885.46, 11869.4, 11874.62,
        ]],
      }
      // let binLiteralValue = idaxData.data[0][2].toString();
      return calculatePriceData(binance, digi)
    }
  })
}

const getStartBaseTime = function () {
  const now = new Date()
  return new Date(moment(now).format('YYYY-MM-DD HH:mm')).getTime()
  // return new Promise((resolve) => {
  //   request.get(idaxTimeBaseUrl, (error, res, body) => {
  //     const response = JSON.parse(body);
  //     if (!error && res.statusCode === 200) {
  //       logger.info('time data:' + JSON.stringify(response));
  //       if (response.code === 10000) {
  //         logger.info('base time data:', response.kline[0][0], ', KST: ', new Date(response.kline[0][0]).toString());
  //         resolve(response.kline[0][0]);
  //       } else {
  //         logger.info('Base time fetch error : ' + idaxTimeBaseUrl);
  //         resolve(0);
  //       }
  //     } else {
  //       resolve(0);
  //     }
  //   });
  // });
}

function calculatePriceData(binance, digiFinex) {
  let count = 0
  let sum = 0
  let d1 = 0
  let d2 = 0

  let av = 0
  let biData = binance
  let digiData = digiFinex
  // logger.info('time', new Date(nowPoint).toString());
  logger.info('===================== calculatePriceData START ======================')
  logger.info('Binance Data    : ', JSON.stringify(biData))
  logger.info('DigiFinex Data : ', JSON.stringify(digiData))
  logger.info('Crawler Cnt  : ', crawlerCounter)
  if (biData.code === IDAX_OK) {
    count++
    const biLiteralValue = biData.kline[0][4].toString()
    const cutBiValue = biLiteralValue.substr(0, biLiteralValue.indexOf('.') + 3)
    const replacedValue = cutBiValue.replace('.', '')

    d1 = Number(replacedValue)
    sum += d1
  }

  // if (binData[0]) {
  //   count++;
  //   const binLiteralValue = binData[0][4].toString();
  //   const cutBinValue = binLiteralValue.substr(0, binLiteralValue.indexOf('.') + 3);
  //   const binReplacedValue = cutBinValue.replace('.', '');
  //
  //   d2 = Number(binReplacedValue);
  //   sum += d2
  // }
  if (digiData.code === 0 && digiData.data) {
    count++
    let digiLiteralValue = digiData.data[0][2].toString()
    if (!digiLiteralValue.split('.')[1]) {
      digiLiteralValue += '.00'
    } else if (digiLiteralValue.split('.')[1].length === 1) {
      digiLiteralValue += '0'
    }
    const cutDigiValue = digiLiteralValue.substr(0, digiLiteralValue.indexOf('.') + 3)
    const digiReplacedValue = cutDigiValue.replace('.', '')

    d2 = Number(digiReplacedValue)
    sum += d2
  }
  if (sum > 0) {
    av = sum / count
    if (av > 0) {
      av = Number((Math.floor(av) / 100).toFixed(2))
    }
  }
  logger.info('calculated binance(%d), digiFinex(%d), average(%d)', d1 / 100, d2 / 100, av)
  logger.info(`===================== calculatePriceData END ======================`)
  return { binance: d1 / 100, idax: d2 / 100, av: av }
}

function generateRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

/**
 *
 * @param nowPoint number
 * @returns {Promise<any[]>}
 */
const getAllKline = async function (nowPoint) {
  digifinexDone = false
  biDone = false
  logger.info(`############################## getAllKline (${new Date()}############################## getAllKline (`)
  logger.info('getAllKline, nowPoint(%d), pointDate(%s)', nowPoint, new Date(nowPoint).toLocaleString())
  if (nowPoint > new Date().getTime()) {
    logger.info(`**** reqTime > current time : ${nowPoint} - ${new Date().getTime()}`)
    return new Promise(function (resolve) {
      resolve({ idax: 0, binance: 0, av: 0 })
    })
  }
  // digiFinex 는 주어진 point 가 시작값이고, idax 는 끝값이다
  const p1 = new Promise(function (resolve) {
    let reqGet = util.format("%s%d", binanceBaseUrl, nowPoint);
    // let reqGet = util.format('%s%d', idaxBaseUrl, nowPoint)
    logger.info('reqGet:   %s', reqGet)
    const startTime = new Date().getTime()
    request.get(reqGet, { timeout: KLINE_API_GET_TIME_OUT }, function (error, res, body) {
      const endTime = new Date().getTime()
      const result = endTime - startTime
      iLastDelayTime = result
      if (result > 800) {
        iEightCount++
      }
      if (result > 1000) {
        iOneMinutesCount++
      }
      if (iMinDelayTime === 0 || iMinDelayTime > result) {
        iMinDelayTime = result
      }
      if (iMaxDelayTime < result) {
        iMaxDelayTime = result
      }
      if (!error && res.statusCode === 200) {
        let obj = JSON.parse(body)
        biDone = true
        resolve(obj)
      } else {
        biDone = true
        resolve({ code: 0 })
      }
    })
  })

  const p2 = new Promise(function (resolve, reject) {
    // let reqGetBinance = util.format("%s%d", binanceBaseUrl, nowPoint);
    const timeStr = nowPoint.toString()
    const newTime = timeStr.substr(0, timeStr.length - 3)
    const eTime = Number(newTime)
    const sTime = eTime - 10
    // https://openapi.digifinex.vip/v3/kline?symbol=btc_usdt&period=1&start_time=1565257310&end_time=1565257320
    const reqGetDigifinex = `https://openapi.digifinex.vip/v3/kline?symbol=btc_usdt&period=1&start_time=${sTime}&end_time=${eTime}`
    logger.info('reqGetDigifinex: %s', reqGetDigifinex)
    const startTime = new Date().getTime()
    request.get(reqGetDigifinex, { timeout: KLINE_API_GET_TIME_OUT }, function (error, res, body) {
      const endTime = new Date().getTime()
      const result = endTime - startTime
      dLastDelayTime = result
      if (result > 800) {
        dEightCount++
      }
      if (result > 1000) {
        dOneMinutesCount++
      }
      if (dMinDelayTime === 0 || dMinDelayTime > result) {
        dMinDelayTime = result
      }
      if (dMaxDelayTime < result) {
        dMaxDelayTime = result
      }
      if (!error && res.statusCode === 200) {
        let obj = JSON.parse(body)
        // const delayTime = generateRandom(20, 80) / 10 * 1000;
        // setTimeout((delayTimeParam) => {
        //   digifinexDone = true
        //   if (delayTimeParam >= 7000) {
        //     // resolve({ code: 0 })
        //     reject({ code: 0 })
        //   } else {
        //     resolve(obj)
        //   }
        // }, delayTime, delayTime)
        digifinexDone = true
        resolve(obj)
      } else {
        digifinexDone = true
        resolve({ code: 0 })
      }
    })
  })

  return Promise.all([p1, p2])
    .then(function (values) {
      logger.info(`===================== getAllKline END ======================`)
      return calculatePriceData({code: 10000, kline: values[0]}, values[1])
    })
    .catch(function (err) {
      //reject(0);
      logger.info('-_-_-_-_-_-_-_- getAllKline catch: ', err)
      klineErrorCount++
      digifinexDone = true
      biDone = true
      return { idax: 0, binance: 0, av: 0 }
    })
}

const getServerTime = async function () {
  let d1 = 0
  let d2 = 0

  const p1 = new Promise(function (resolve) {
    request.get(binanceTime, { timeout: API_GET_TIME_OUT }, function (error, res, body) {
      if (!error && res.statusCode === 200) {
        let obj = JSON.parse(body)
        resolve(obj)
      } else {
        resolve({ code: 0 })
      }
    })
  })

  const p2 = new Promise(function (resolve) {
    request.get(digiFinexTime, { timeout: API_GET_TIME_OUT }, function (error, res, body) {
      if (!error && res.statusCode === 200) {
        resolve(body)
      } else {
        resolve({ code: 0 })
      }
    })
  })

  return Promise.all([p1, p2]).then(function (values) {
    let data1 = values[0]
    let data2 = values[1]
    //logger.info("all come, cbene:(%s),obtc(%s),idax(%s)",JSON.stringify(data1),JSON.stringify(data2),JSON.stringify(data3));

    if (data1.serverTime) {
      d1 = Number(data1.serverTime)
    }

    if (data2.code === DIGI_FINEX_OK) {
      d2 = Number(data2.server_time)
    }

    const now = (new Date()).getTime()
    logger.info('----------------------------------------------------------------------------------------')
    logger.info('time,binance (%d), digifinex (%d), localTime (%d)', d1, d2, now)
    logger.info('binance      (%s)', (new Date(d1)).toString())
    logger.info('digiFinex    (%s)', (new Date(d2)).toString())
    logger.info('localTime    (%s)', (new Date(now)).toString())
    logger.info('----------------------------------------------------------------------------------------')
    return { bnTime: d1, idTime: d2 }

  }).catch(function (err) {
    //reject(0);
    logger.info('catch: ', err)
    return { bnTime: 0, idTime: 0 }
  })
}

function printTimeCheckSummary() {
  logger.info(`------------------------------- CHECK SUMMARY (${new Date(startBaseTime).toLocaleString()}) --------------------------------`)
  logger.info(`### Crawling count : ${(crawlerCounter - 1) === 0 ? 120 : (crawlerCounter - 1)}`)
  logger.info(`### IDAX     min time : ${iMinDelayTime}`)
  logger.info(`### IDAX     max time : ${iMaxDelayTime}`)
  logger.info(`### IDAX    last time : ${iLastDelayTime}`)
  logger.info(`### IDAX    800 < cnt : ${iEightCount}`)
  logger.info(`### IDAX   1sec < cnt : ${iOneMinutesCount}`)
  logger.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
  logger.info(`### Digi     min time : ${dMinDelayTime}`)
  logger.info(`### Digi     max time : ${dMaxDelayTime}`)
  logger.info(`### Digi    last time : ${dLastDelayTime}`)
  logger.info(`### Digi    800 < cnt : ${dEightCount}`)
  logger.info(`### Digi   1sec < cnt : ${dOneMinutesCount}`)
  logger.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
  logger.info(`### kline Error Count : ${klineErrorCount}`)
  logger.info('------------------------------- ============= --------------------------------')
}

module.exports = { beforeCrawling, startCrawling, getPriceData, increaseCount }
